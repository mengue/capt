
Instru��es para compila��o (conforme documento de  especifica��o)
=================================================================

3. Linguagem e Compilador

Ser� utilizada a linguagem C++ para o desenvolvimento do jogo. Aplica��o tipo console.

A ferramenta Visual Studio 2010 ser� utilizada como ambiente de desenvolvimento e compilador para o jogo.

Ser� utilizado o reposit�rio do GitBucket que permite o acompanhamento, compartilhamento e controle de vers�es do projeto.

Arquivos necess�rios para o projeto:
�	CapT.cpp => c�digo fonte (necess�rio para compila��o)
�	CapT.exe => programa execut�vel (necess�rio para execu��o)
�	CapT.bmp => imagens do jogo (necess�rio para execu��o)
�	CapT.txt => labirinto do jogo (necess�rio para execu��o)


