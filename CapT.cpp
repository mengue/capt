/********************************
* CapT.C                        *
*                               *
* by: Leandro Mengue (c) - 2015 *
********************************/
#include "stdafx.h"
#include <dos.h>
#include <io.h>
#include <malloc.h>
#include <windows.h>
#include <time.h>

int say(int,int,int,int);
int xsay(int,int,int);
int pix(int,int);
int pix2(int,int,int,int);
int direcao(int,int,int *,int *);
int direcao2(int,int,int *,int *,int,int);
int morre( int *, int *, int *, int *);
int monta_tela();

int vidas=3, tpix=3, tamtela;

int tpos[4] = {0,1,14,39};
unsigned char *base[20];

char *num;

unsigned char *sprites, *tela;

class Aluno {

        public: int lin, col, car, pos, tmp, alin, acol;

        public: void init() {
                lin=81;
                col=138;
                car=2;
                tmp=5;
                pos=0;
        }

        public: void wsay() { if (acol!=col || alin!=lin) say(lin,col,car,pos); alin=lin; acol=col; }
};

class Professor {

        public: int lin, col, car, pos, tmp, v, alin, acol;

        public: void init() {
                lin=165;
                col=42;
                car=4;
                pos=0;
                tmp=35;
                v=0;
        }

        public: void wsay() { if (acol!=col || alin!=lin) say(lin,col,car,pos); alin=lin; acol=col; }

        public: void limpa() { say(lin,col,0,0); }
};

class Morcego {

        public: int lin, col, car, pos, alin, acol;

        public: void init() {
                lin=30;
                col=246;
                car=7;
                pos=0;
        }

        public: void wsay() { if (acol!=col || alin!=lin) say(lin,col,car,pos); alin=lin; acol=col; }

        public: void limpa() { say(lin,col,0,0); }
};

class Aranha {

        public: int lin, col, car, alin, acol, pos;

        public: void init() {
                lin=45;
                col=42;
                car=10;
				pos=0;
        }

        public: void wsay() { if (acol!=col || alin!=lin) say(lin,col,car,pos); alin=lin; acol=col; }

        public: void limpa() { say(lin,col,0,0); }
};

class Tiro {

        public: int lin, col, car, tmp, n, a;

        public: void init() {
                lin=0;
                col=0;
                car=11;
                tmp=5;
                n=0;
                a=0;
        }
        
        public: void wsay() { xsay(lin,col,car); }

        public: void limpa() { xsay(lin,col,0); }
};

class Arma {

        public: int lin, col, car, tmp, n, a, acol, alin;

        public: void init() {
                lin=0;
                col=0;
                car=11;
                tmp=5;
                n=0;
                a=0;
        }
        
        public: void wsay() { if (acol!=col || alin!=lin) say(lin,col,car,0); alin=lin; acol=col; }

        public: void limpa() { say(lin,col,0,0); }
};

HWND console_handle;
HDC device_context;
HPEN pen1,pen2;
HBRUSH bru;

//__________________________________________________________________________________________________________________
int _tmain(int argc, _TCHAR* argv[])
{
        long tam;

        unsigned char *buf, carx;

        int han,key,i,j,k,x=0,y=30;

        int ilr=0,icr=1,ila=1,ica=0,ilm1=0,icm1=1,ilm2=0,icm2=1,ict=0,ilt=0,icm=0,ilm=0,bume=1;

        int score=0,linta=0,colta=0,linma1=0,colma1=0,linma2=0,colma2=0,linaa=0,colaa=0,linra=0,colra=0,linha=0,colha=0,colma=0,linma=0,sono=0,ascore=0;

        int tmpbic=50,tempo=0,vel=100,fase=0;

        Aluno *aluno = new Aluno;
        Professor *professor = new Professor;
        Morcego *morcego1 = new Morcego;
        Morcego *morcego2 = new Morcego;
        Aranha *aranha = new Aranha;
        Tiro *tiro = new Tiro;
        Tiro *missel = new Tiro;
        Arma *arma= new Arma;

        aluno->init();
        professor->init();
        morcego1->init();
        morcego2->init();
        tiro->init();
        aranha->init();

        morcego2->lin=60;
        morcego2->col=246;

        arma->lin=120;
        arma->col=246;
        arma->car=9;

  num=(char *)malloc(10);

  console_handle = GetConsoleWindow();
  //MoveWindow(console_handle, 0, 0, 1000, 600, TRUE); 
  device_context = GetDC(console_handle);

  pen1=CreatePen(PS_SOLID,2,RGB(255,0,0));
  pen2=CreatePen(PS_SOLID,2,RGB(0,0,0));
  bru=CreateSolidBrush(RGB(0,0,0));
  SelectObject(device_context,pen1);
  SelectObject(device_context,bru);

  srand( (unsigned)time( NULL ) );

  han=open("CapT.txt",0x8000);
  tamtela=filelength(han);
  tela=(unsigned char *)malloc(tamtela+1);
  *(tela+tamtela)=0;
  read(han,tela,tamtela);
  close(han);

  han=open("CapT.bmp",0x8000);  // 304x981 , 4x3 , 1/14/39
  tam=filelength(han);
  sprites=(unsigned char *)malloc(tam);
  buf=sprites;
  while(tam>0) {
          i=read(han,buf,(tam>10000 ? 10000 : tam));
          tam-=i;
          buf+=i;
  }
  close(han);

  buf-=304*3*3-3;
  for(i=0; i<20; i++) {
          base[i]=buf;
          buf-=49*304*3;
  }
  monta_tela(); 

  Beep(1000,100) ; Beep(9000,100); Beep(4000,50); Beep(9000,100);

  printf("       SCORE %05d   CAP THE THESIS FIRE %1d\r",score,tiro->n);
  morre(&score,&aluno->tmp,&aluno->lin,&aluno->col);
  for(i=0; i<vidas; i++) say(185,i*12,4,0);

  while(1)
  {
    (tiro->n!=0 ? 0 : arma->wsay());
    (ilt==0 && ict==0 ? 0 : xsay(tiro->lin,tiro->col,tiro->car));
    (ilm==0 && icm==0 ? 0 : xsay(missel->lin,missel->col,missel->car));
    morcego1->wsay();
    morcego2->wsay();
    (professor->lin==0 ? 0 : professor->wsay());
    aranha->wsay();
    aluno->wsay();

    key=0;

    if ( (tempo+3)%aluno->tmp==0 )
     {
	 key=( GetAsyncKeyState(VK_ESCAPE) & 0x8000 ? 27 : key );
	 key=( GetAsyncKeyState(VK_DOWN) & 0x8000 ? 80 : key );
	 key=( GetAsyncKeyState(VK_UP) & 0x8000 ? 72 : key );
	 key=( GetAsyncKeyState(VK_RIGHT) & 0x8000 ? 77 : key );
	 key=( GetAsyncKeyState(VK_LEFT) & 0x8000 ? 75 : key );
	 key=( GetAsyncKeyState(VK_LSHIFT) & 0x8000  ||  GetAsyncKeyState(VK_RSHIFT) & 0x8000 ? key+100 : key );
     key=( GetAsyncKeyState(49) & 0x8000 ? 53 : key );
     key=( GetAsyncKeyState(50) & 0x8000 ? 55 : key );

	 if (sono>0 || aluno->tmp==99)
      {
        sono--;
        key=( key==27 ? key : 0);
		if (sono==0) { aluno->car=2; aluno->alin=0; }
      }
      else
      {
        aluno->pos=(aluno->col+aluno->lin)%4;
      }
      if (score!=ascore || tiro->a!=tiro->n)
      {
        tiro->a=tiro->n;
        ascore=score;
        printf("       SCORE %05d CAP THE THESIS   FIRE %1d\r",score,tiro->n);
      }
    }

    professor->pos=(professor->col+professor->lin)%4;
    morcego1->pos=(morcego1->col+morcego1->lin)%4;
    morcego2->pos=(morcego2->col+morcego2->lin)%4;

    if ( (aluno->lin+14 >= arma->lin  &&  aluno->lin <= arma->lin+14) && (aluno->col+11 >= arma->col  &&  aluno->col <= arma->col+11 ) && tiro->n==0 )
    {
      arma->limpa();
      tiro->n=7;
    }

    if ( (aluno->lin+14 >= professor->lin  &&  aluno->lin <= professor->lin+14) && (aluno->col+11 >= professor->col  &&  aluno->col <= professor->col+11 ) )
    {
      morre(&score,&aluno->tmp,&aluno->lin,&aluno->col);
    }

    if ( ( (aluno->lin+14 >= morcego1->lin+3 && aluno->lin <= morcego1->lin+8) && (aluno->col+11 >= morcego1->col  &&  aluno->col <= morcego1->col+11 ) ) ||
         ( (aluno->lin+14 >= morcego2->lin+3 && aluno->lin <= morcego2->lin+8) && (aluno->col+11 >= morcego2->col  &&  aluno->col <= morcego2->col+11 ) ) ||
         ( (aluno->lin+14 >= aranha->lin     && aluno->lin <= aranha->lin +14) && (aluno->col+11 >= aranha->col    &&  aluno->col <= aranha->col+11 ) ) )
    {
      sono=50;
      aluno->car=12;
	  aluno->alin=0;
    }

   if (key==27) break;
  
   if ( key==72 || key==80 || key==77 || key==75 )
   {

     ( key==72 && pix(aluno->lin-1,aluno->col+1)==0 && pix(aluno->lin-1,aluno->col+6)==0 && (aluno->col-30+1)%12<3 ? aluno->lin--,   ((aluno->col-30)%12!=0 ?  aluno->col-=(aluno->col-30+1)%12-1 : 0 ) : 0);

     ( key==80 && pix(aluno->lin+15,aluno->col+1)==0 && pix(aluno->lin+15,aluno->col+6)==0 && (aluno->col-30+1)%12<3 ? aluno->lin++, ((aluno->col-30)%12!=0 ?  aluno->col-=(aluno->col-30+1)%12-1 : 0 )  : 0);

     ( key==77 && pix(aluno->lin+1,aluno->col+12)==0 && pix(aluno->lin+7,aluno->col+12)==0 && (aluno->lin+1)%15<3 ? aluno->col++, (aluno->lin%15!=0 ? aluno->lin-=(aluno->lin+1)%15-1 : 0) : 0);

     ( key==75 && pix(aluno->lin+1,aluno->col-1)==0 && pix(aluno->lin+7,aluno->col-1)==0 && (aluno->lin+1)%15<3 ? aluno->col--, (aluno->lin%15!=0 ? aluno->lin-=(aluno->lin+1)%15-1 : 0) : 0);

     if (aluno->col==138 && aluno->lin==81)
     {
       say(75,138,0,0);
     }

     if (aluno->col==138 && aluno->lin==82)
     {
       say(75,138,18,0);
       say(81,138,0,0);
     }
   }

   if ( ict==0 && ilt==0 && tiro->n!=0)
   {
     if ( key==172 || key==180 || key==177 || key==175 )
     {
       Beep(9000,100);
       ( key==180 ? ilt=1  : 0);
       ( key==172 ? ilt=-1 : 0);
       ( key==175 ? ict=-1 : 0);
       ( key==177 ? ict=1  : 0);
       tiro->n--;
       tiro->lin=aluno->lin+ilt*11+6;
       tiro->col=aluno->col+ict*9+4;
       if ( pix(tiro->lin+(ilt+1)*5/2-1, tiro->col+(ict+1)*5/2-1 )!=0 )
       {
         tiro->lin=tiro->col=ict=ilt=0;
       }
     }

     if( tiro->n==0 && (ict!=0 || ilt!=0))
     {
       arma->lin=24;
       arma->col=35;
       while( pix(arma->lin,arma->col)!=0 )
       {
         arma->col=(rand()%20)*12+30;
         arma->lin=(rand()%10+1)*15;
       }
     }
   }

   if ( (ict!=0 || ilt!=0) )
   {
     if ( (tiro->lin+3 >= professor->lin  &&  tiro->lin <= professor->lin+14) && (tiro->col+3 >= professor->col  &&  tiro->col <= professor->col+11 ) )
     {
       if (professor->v==0)
       {
///         sound(2000) ; delay(10) ; sound(2500) ; delay(10) ; nosound();
         professor->limpa();
         score+=500;
         professor->lin=professor->col=ilr=icr=0;
         x=fase;
         fase = ( score<3000 ? 0 : ( score<6000 ? 1 : ( score<9000 ? 2 : ( score<12000 ? 3 : 4 ))));
         professor->v = (fase<2 ? 0 : 2);
         professor->tmp-= (x!=fase ? 2 : 0);
         if (fase>2) professor->car=0;
       }
       else
       {
         xsay(tiro->lin,tiro->col,0);
         tiro->lin=tiro->col=ilt=ict=0;
         professor->v--;
         for(i=0; i<5; i++)
         {
           Beep(9000,30);
           say(professor->lin,professor->col,19,0);
           Beep(7000,30);
           professor->wsay();
         }
       }
     }

     if ( (tiro->lin+3 >= aranha->lin  &&  tiro->lin <= aranha->lin+14) && (tiro->col+3 >= aranha->col  &&  tiro->col <= aranha->col+11 ) )
     {
       aranha->limpa();
       score+=50;
       aranha->lin=120;
       aranha->col=42;
       ila=1;
       ica=0;
     }

     if ( (tiro->lin+3 >= morcego1->lin+2  &&  tiro->lin <= morcego1->lin+8) && (tiro->col+3 >= morcego1->col  &&  tiro->col <= morcego1->col+11 ) )
     {
       morcego1->limpa();
       score+=50;
       morcego1->lin=15;
       morcego1->col=270;
       ilm1=0;
       icm1=1;
     }

     if ( (tiro->lin+3 >= morcego2->lin+2  &&  tiro->lin <= morcego2->lin+8) && (tiro->col+3 >= morcego2->col  &&  tiro->col <= morcego2->col+11 ) )
     {
       morcego2->limpa();
       score+=50;
       morcego2->lin=45;
       morcego2->col=270;
       ilm2=0;
       icm2=1;
     }

     if ( (tiro->lin+3 >= missel->lin  &&  tiro->lin <= missel->lin+3) && (tiro->col+3 >= missel->col  &&  tiro->col <= missel->col+3 ) )
     {
       xsay(missel->lin,missel->col,0);
       xsay(tiro->lin,tiro->col,0);
       if (fase<4)
       {
         tiro->lin+=ilt*4;
         tiro->col+=ict*4;
         missel->lin+=ilm*4;
         missel->col+=icm*4;
       }
       else
       {
         tiro->lin=tiro->col=missel->lin=missel->col=ilt=ict=ilm=icm=0;
       }
     }
   }
   else
   {
     if ( professor->lin==0 )
     {
       professor->lin=165;
       professor->col=42;
       icr=1;
     }
   }

   if ( (icm!=0 || ilm!=0) )
   {
     if ( (missel->lin+3 >= aranha->lin  &&  missel->lin <= aranha->lin+14) && (missel->col+3 >= aranha->col  &&  missel->col <= aranha->col+11 ) )
     {
       aranha->limpa();
       aranha->lin=120;
       aranha->col=42;
       ila=1;
       ica=0;
     }

     if ( (missel->lin+3 >= morcego1->lin+2  &&  missel->lin <= morcego1->lin+8) && (missel->col+3 >= morcego1->col  &&  missel->col <= morcego1->col+11 ) )
     {
       morcego1->limpa();
       morcego1->lin=15;
       morcego1->col=270;
       ilm1=0;
       icm1=1;
     }

     if ( (missel->lin+3 >= morcego2->lin+2  &&  missel->lin <= morcego2->lin+8) && (missel->col+3 >= morcego2->col  &&  missel->col <= morcego2->col+11 ) )
     {
       say(morcego2->lin,morcego2->col,0,0);
       morcego2->lin=45;
       morcego2->col=270;
       ilm2=0;
       icm2=1;
     }
   }

   if ( (icm!=0 || ilm!=0) )
   {
     if ( (missel->lin+4 >= aluno->lin  &&  missel->lin-1 <= aluno->lin+14) && (missel->col+4 >= aluno->col  &&  missel->col-1 <= aluno->col+11 ) )
     {
        morre(&score,&aluno->tmp,&aluno->lin,&aluno->col);
     }
   }

   if ( pix2( tiro->lin + ( ilt==0 ? 0 : ( ilt == 1 ? 4 : -1)), tiro->col + ( ict==0 ? 0 : ( ict == 1 ? 4 : -1)), tiro->lin + ( ilt==0 ? 3 : ( ilt == 1 ? 4 : -1)), tiro->col + ( ict==0 ? 3 : ( ict == 1 ? 4 : -1)) ) )
   {
     ( pix2(tiro->lin,tiro->col,tiro->lin+3,tiro->col+3)==0 ? xsay(tiro->lin,tiro->col,0) : 0);
     tiro->lin=tiro->col=ict=ilt=0;
   }

   direcao(aranha->lin,aranha->col,&ila,&ica);
   direcao(morcego1->lin,morcego1->col,&ilm1,&icm1);
   direcao(morcego2->lin,morcego2->col,&ilm2,&icm2);

   if (tempo%professor->tmp==0)
   {
     ( fase>1 ? direcao2(professor->lin,professor->col,&ilr,&icr,aluno->lin,aluno->col) : direcao(professor->lin,professor->col,&ilr,&icr));
     professor->lin+=ilr;
     professor->col+=icr;
   }

   if (icm==0 && ilm==0 && fase!=0 && professor->lin!=0 )
   {
     icm=rand()%3-1;
     ilm=(rand()%3-1)*(1-abs(icm));
     missel->lin=professor->lin+ilm*11+6;
     missel->col=professor->col+icm*9+4;
     bume=1;
     if ( pix(missel->lin,missel->col) || pix2( missel->lin + ( ilm==0 ? 0 : ( ilm == 1 ? 4 : -1)), missel->col + ( icm==0 ? 0 : ( icm == 1 ? 4 : -1)), missel->lin + ( ilm==0 ? 3 : ( ilm == 1 ? 4 : -1)), missel->col + ( icm==0 ? 3 : ( icm == 1 ? 4 : -1)) ) )
     {
       bume=missel->lin=missel->col=icm=ilm=0;
     }
     else
     {
       say(professor->lin,professor->col,19,0);
       Sleep(10);
       professor->wsay();
     }
   }

   if ( pix2( missel->lin + ( ilm==0 ? 0 : ( ilm == 1 ? 4 : -1)), missel->col + ( icm==0 ? 0 : ( icm == 1 ? 4 : -1)), missel->lin + ( ilm==0 ? 3 : ( ilm == 1 ? 4 : -1)), missel->col + ( icm==0 ? 3 : ( icm == 1 ? 4 : -1)) ) )
   {
     xsay(missel->lin,missel->col,0);
     missel->lin=missel->col=icm=ilm=0;
   }

   if ((tempo+1)%tmpbic==0)
   {
     aranha->lin+=ila;
     aranha->col+=ica;

     morcego1->lin+=ilm1;
     morcego1->col+=icm1;

     morcego2->lin+=ilm2;
     morcego2->col+=icm2;
   }

   if ((tempo+2)%tiro->tmp==0)
   {
     tiro->lin+=ilt;
     tiro->col+=ict;

     if( fase>3 && bume==1 && ( icm!=0 ? (missel->col-30)%12==6 : ( ilm!=0 ? missel->lin%15==7 : 0)) )
     {
       x=ilm;
       y=icm;
       direcao2(missel->lin-(missel->lin%15),missel->col-((missel->col-30)%12),&ilm,&icm,aluno->lin,aluno->col);
       bume=( ilm!=x || icm!=y ? 0 : 1);
     }
     missel->col+=icm;
     missel->lin+=ilm;
   }

   if (key==53 && tpix<3) { tpix++; monta_tela(); }
   if (key==55 && tpix>1) { tpix--; monta_tela(); }

   tempo=(tempo>30000 ? 0 : tempo+1);
  }
//  sound(900); delay(200); sound(400); delay(200); sound(900); delay(200); nosound();
}

//__________________________________________________________________________________________________________________
int pix(int lin, int col)
{
  return( GetPixel(device_context,col*tpix,lin*tpix) );
}

//__________________________________________________________________________________________________________________
int pix2(int li, int ci, int lf, int cf)
{
  int lin,col,bit=0,inc;

  for(lin=li ; lin<=lf && bit==0 ; lin++)
  {
    inc=(lin==li || lin==lf || ci==cf ? 1 : cf-ci);
    for(col=ci ; col<=cf && bit==0 ; col+=inc)
    {
      bit = GetPixel(device_context,col*tpix,lin*tpix);
    }
  }
  return( bit );
}

//__________________________________________________________________________________________________________________
int say(int lin, int col, int car, int pos)
{
  unsigned char *ss, *buf;
  int i,j,mov,bit,p;
  long cor;

  bit=(col%4)*2;

  SelectObject(device_context,pen1);

  for(i=0; i<15; i++)
  {
          for(j=0; j<12; j++)
          {
                  buf=base[car]-i*304*3+j*3+pos*75*3;
                  cor=RGB(buf[2],buf[1],buf[0]);

                  SetPixel(device_context,col*tpix+j*tpix,lin*tpix+i*tpix,cor);
                  if (tpix>1) {
                        SetPixel(device_context,col*tpix+j*tpix,lin*tpix+i*tpix+1,cor);
                         SetPixel(device_context,col*tpix+j*tpix+1,lin*tpix+i*tpix,cor);
                         SetPixel(device_context,col*tpix+j*tpix+1,lin*tpix+i*tpix+1,cor);
                  }
                  if (tpix>2) {
                         SetPixel(device_context,col*tpix+j*tpix+2,lin*tpix+i*tpix,cor);
                         SetPixel(device_context,col*tpix+j*tpix+2,lin*tpix+i*tpix+1,cor);
                         SetPixel(device_context,col*tpix+j*tpix+0,lin*tpix+i*tpix+2,cor);
                         SetPixel(device_context,col*tpix+j*tpix+1,lin*tpix+i*tpix+2,cor);
                         SetPixel(device_context,col*tpix+j*tpix+2,lin*tpix+i*tpix+2,cor);
                  }
          }
  }
  SelectObject(device_context,pen1);
  return(0);
}

//__________________________________________________________________________________________________________________
int xsay(int lin, int col, int car)
{
  unsigned char *ss;
  int i,j,mov,bit;
  long cor;

  cor=(car==0 ? 0 : RGB(255,255,255));
  for(i=0; i<4; i++)
  {
	for(j=0; j<4; j++)
	{
	  bit=(i==0 || i==3 || j==0 || j==3 ? 0 : cor);
 	  SetPixel(device_context,(col+j)*tpix+0,(lin+i)*tpix+0,bit);
      if (tpix>1) 
	  {
		 SetPixel(device_context,(col+j)*tpix+1,(lin+i)*tpix+0,bit);
		 SetPixel(device_context,(col+j)*tpix+0,(lin+i)*tpix+1,bit);
		 SetPixel(device_context,(col+j)*tpix+1,(lin+i)*tpix+1,bit);
      }
      if (tpix>2) 
	  {
		 SetPixel(device_context,(col+j)*tpix+2,(lin+i)*tpix+0,bit);
		 SetPixel(device_context,(col+j)*tpix+2,(lin+i)*tpix+1,bit);
		 SetPixel(device_context,(col+j)*tpix+2,(lin+i)*tpix+2,bit);
		 SetPixel(device_context,(col+j)*tpix+0,(lin+i)*tpix+2,bit);
		 SetPixel(device_context,(col+j)*tpix+1,(lin+i)*tpix+2,bit);
      }
   }
  }
  return(0);
}

//__________________________________________________________________________________________________________________
int direcao( int linrob, int colrob, int *il, int *ic)
{
  char *ini;
  int i,w,ilr,icr,k=0;

  ilr=*il * 8 + 7;
  icr=(*ic+1)*13/2-1;
  ini=num;

  if ( *ic!=0 && (colrob-30)%12==0 && ( (w=pix(linrob,colrob+icr))!=0 ||
                pix(linrob-1,colrob)==0 || pix(linrob+15,colrob)==0 ) )
  {
    (pix(linrob+15,colrob)==0 ? *(num++)=1 : 0);
    (pix(linrob-1,colrob)==0 ? *(num++)=2 : 0);
    (pix(linrob,colrob-1)==0 && (*ic!=1  || w!=0) ? *(num++)=3 : 0);
    ((pix(linrob,colrob+12)==0 && (*ic!=-1 || w!=0)) ? *(num++)=4 : 0);
  }
  else
  {
    if (*il!=0 && linrob%15==0 && ((w=pix(linrob+ilr,colrob))!=0 ||
                  pix(linrob,colrob-1)==0 ||
                  pix(linrob,colrob+12)==0) )
    {
      (pix(linrob+15,colrob)==0 && (*il!=-1 || w!=0) ? *(num++)=1 : 0);
      (pix(linrob-1,colrob)==0 && (*il!=1  || w!=0) ? *(num++)=2 : 0);
      (pix(linrob,colrob-1)==0 ? *(num++)=3 : 0);
      (pix(linrob,colrob+12)==0 ? *(num++)=4 : 0);
    }
    else
    {
      return(0);
    }
  }
  if (num-ini)
  {
    i=*(ini + (rand() % (num-ini)) );
    *ic = ( i==3 ? -1 : ( i==4 ?  1 : 0 ));
    *il = ( i==1 ?  1 : ( i==2 ? -1 : 0 ));
  }
  num=ini;
}

//__________________________________________________________________________________________________________________
int direcao2( int linrob, int colrob, int *il, int *ic, int linhom, int colhom)
{
  int i,w,z,ilr,icr,dc,dl;

  ilr=*il * 8 + 7;
  icr=(*ic+1)*13/2-1;

  if ( ( *ic!=0 && (colrob-30)%12==0 && ( (w=pix(linrob,colrob+icr))!=0 ||
                pix(linrob-1,colrob)==0 || pix(linrob+15,colrob)==0 ) ) ||
       (*il!=0 && linrob%15==0 && ((z=pix(linrob+ilr,colrob))!=0 ||
                  pix(linrob,colrob-1)==0 ||
                  pix(linrob,colrob+12)==0) ) )
  {
    dl=linhom-linrob;
    dc=colhom-colrob;
    if ( ( ((abs(dl)-1)*(abs(dl)-1) + dc*dc) < (dl*dl + (abs(dc)-1)*(abs(dc)-1)) && (*il != (dl>0 ? -1 : 1)) ) || (*ic==(dc>0 ? -1 : 1)) )
    {
      dl = ( pix(linrob+( (dl>0 ? 1 : -1) )*8+7,colrob)==0 ? (dl>0 ? 1 : -1) : 0);
      dc = 0;
    }
    else
    {
      dc = ( pix(linrob,colrob+( (dc>0 ? 1 : -1) +1)*13/2-1)==0 ? (dc>0 ? 1 : -1) : 0);
      dl = 0;
    }
    if (dl==0 && dc==0)
    {
      direcao(linrob,colrob,il,ic);
    }
    else
    {
      *ic=dc;
      *il=dl;
    }
  }
  return(0);
}

//__________________________________________________________________________________________________________________
int morre( int *score, int *tmphom, int *linhom, int *colhom )
{
  int i;

  if (*tmphom!=99)
  {
///    sound(300); delay(100); sound(100); delay(100); sound(300); delay(100); nosound();
    if (vidas==0)
    {
      printf("       SCORE %05d    GAME OVER    \r",*score);
      *tmphom=99;
//	  aluno->car=0;
    }
    else
    {
      say(75,138,18,0);
      say(*linhom,*colhom,0,0);
      vidas--;
      Sleep(500);
      *linhom=82;
      *colhom=138;
      (vidas<2 ? say(185,vidas*12,0,0) : 0);
    }
  }
  return(0);
}

//__________________________________________________________________________________________________________________
int monta_tela()
{
  int i,y=0,x=0;

  SelectObject(device_context,pen2);
  Rectangle(device_context,0,0,2000,2000);
  for(i=8; i<tamtela; i++)
  {
    ( *(tela+i)==219 || *(tela+i)=='#' ? say(x,y,1,0) : 0);
    y+=12;
    if (*(tela+i)==13)
    {
      x+=15;
      y=30;
      i++;
    }
  }
  return(0);
}

